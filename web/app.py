"""VERSION CHECK NOT_A_DRAFT"""
from flask import Flask, render_template

app = Flask(__name__)


#STATUS_OK = "HTTP/1.0 200 OK\n\n"
#STATUS_FORBIDDEN = "HTTP/1.0 403 Forbidden\n\n"
#STATUS_NOT_FOUND = "HTTP/1.0 404 Not Found\n\n"
#STATUS_NOT_IMPLEMENTED = "HTTP/1.0 401 Not Implemented\n\n"

@app.route("/") #no name, forbidden
def index():
    return forbidden(403)


@app.errorhandler(403)
def forbidden(error):
    return render_template("403.html", title="Forbidden"), 403


@app.errorhandler(404)
def not_found(error):
    return render_template("404.html", title="Not Found"), 404


@app.route('/<path:path>')
def Find_the_Path(path):
    if ("~" in path) or ("//" in path) or (".." in path):  #illeagal name, forbidden
        return forbidden(403)
    else:
        try:    # handle the case when incorrect web name accessed, turn to not_found_page
            return render_template(path), 200
        except:
            return not_found(404) #not found name, file not found



if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
